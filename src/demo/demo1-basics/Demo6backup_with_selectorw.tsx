import React, { FormEvent, useState } from 'react';
import { atom, selector, useRecoilCallback, useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';

/**
 * Description
 * Use the set method in selectors
 */
type User = { id: number, name: string, subscribe: boolean }

const currentUsers = selector<any>({
  key: 'Users',
  get: async ({ get }) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    return response.json()
  },
  set:  async ({  set }, value) => {
    console.log('qui', value)
    switch(value.method) {
      case 'DELETE':
        const response = await fetch('https://jsonplaceholder.typicode.com/users/1', { method: 'DELETE'})
        // return response.json()
        console.log('q', currentUsers)
        set( currentUsers, [])
        return []
        break;
    }
  }
})
///// ROOT COMPO /////
export const Demo6: React.FC = () => {
  return <React.Suspense fallback={<div>Loading...</div>}>
  <h1>Demo6</h1>
    <UserForm />
    <UsersList />
  </React.Suspense >
};

///// COMPONENTS /////
function UserForm() {
  const [value, setValue] = useState<string>('')
  /*const [users, setUsers] = useRecoilState<User[]>(usersState)*/

  function submit(e: FormEvent) {
    e.preventDefault();
    // setUsers([...users, { name: value, subscribe: true }]);
    setValue('')
  }

  return (
    <form onSubmit={submit}>
      <input type="text" value={value} onChange={e => setValue(e.target.value)} placeholder="Write something and press ENTER" />
    </form>
  )
}

function UsersList() {
  // const users = useRecoilValue(currentUsers)
  const [users, setUsers] = useRecoilState(currentUsers)
  const writeUsers = useSetRecoilState(currentUsers)

  function deleteHandler(id: number) {
    writeUsers([])
    // setUsers({ method: 'POST', value })
  }

  const deleteMe = useRecoilCallback(({set, snapshot}) => async (value) => {
    const numItemsInCart = await snapshot.getPromise(currentUsers);
    console.log('Items in cart: ', numItemsInCart, value);
    // set(currentUsers, { method: 'POST', value })

  });

  return <div>
    {
      users.map((u: User, index: number) => {
        return <li key={index} onClick={() => deleteHandler(u.id)}>{u.name}</li>
      })
    }
  </div>
}
