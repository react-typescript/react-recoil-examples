import React, { FormEvent, useState } from 'react';
import { atom, selector, useRecoilState, useRecoilValue } from 'recoil';

/**
 * Description: SELECTORS
 * Use Recoil hooks to write state
 * and a selector to read it
 * BUG: "Warning: Cannot update a component (`Batcher`) while rendering a different component (`UsersList`). To locate the bad setState() call inside `UsersList`, follow the stack trace as described in https://fb.me/setstate-in-render"
 * It should be fixed by the libraries (React and recoil). We cannot do anything
 * Read this: https://fb.me/setstate-in-render
 * And this:  https://github.com/facebookexperimental/Recoil/issues/12
 */

///// ATOM /////
const usersState = atom<string[]>({
  key: 'usersState3',
  default: [],
});

const getUsersLengthState = selector({
  key: 'usersLengthState',
  get: ({get}) => {
    return get(usersState).length
  }
})

///// ROOT COMPO /////
export const Demo3: React.FC = () => {
  return <div>
    <UserForm />
    <UsersList />
  </div>
};

///// COMPONENTS /////
function UserForm() {
  // Internal state
  const [value, setValue] = useState<string>('')

  // Recoil shared state
  const [users, setUsers] = useRecoilState<string[]>(usersState)

  function submit(e: FormEvent) {
    e.preventDefault();
    setUsers([...users, value]);
    setValue('')
  }

  return (
    <form onSubmit={submit}>
      <input type="text" value={value} onChange={e => setValue(e.target.value)} placeholder="Write & press ENTER" />
    </form>
  )
}

function UsersList() {
  const totalUsers = useRecoilValue(getUsersLengthState);
  const users = useRecoilValue(usersState);

  return <div>
    There are {totalUsers} users
    {users.map((u, index) => <li key={index}>{u}</li>)}

  </div>
}
