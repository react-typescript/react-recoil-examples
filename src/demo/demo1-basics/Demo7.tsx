import React from 'react';
import {
  selector,
  useRecoilValue,
  useRecoilValueLoadable,
} from 'recoil';
import { ErrorBoundary } from './ErrorBoundary';

/**
 * Description: ASYNC, useRecoilValueLoadable and ErrorBoundaries
 * Async queries (delay simulation)
 * Handle error boundaries
 * TRY: try to set a wrong URL to see how errors works
 */
type User = { id: number, name: string, subscribe: boolean }

const currentUsersState = selector<User[]>({
  key: 'remainingUsers7',
  get: async ({ get }) => {
    // Simulate delay
    return new Promise<User[]>((res, rej) => {
      setTimeout(async () => {
        try {
          const response = await fetch('https://jsonplaceholder.typicode.com/users/')
          // try this to simulate error
          // const response = await fetch('https://jsonplaceholder.typicodex.com/users/')
          if (response.status !== 200) {
            rej('errore!');
          }
          res( await response.json() );
        } catch(e) {
          rej('errore!')
        }
      }, 2000)
    })
  },
})

///// ROOT COMPO /////
export const Demo7: React.FC = () => {
  const users = useRecoilValueLoadable(currentUsersState)

  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      {users.state === 'loading' && <div>Loading data</div>}

      <ErrorBoundary>
        <UsersListCounter />
        {users.state === 'hasValue' && <UsersList users={users.contents}/>}
      </ErrorBoundary>

      {users.state === 'hasError' && <div>Data cannot be loaded</div>}
    </React.Suspense >
  )
};


////// CHILD COMPONENTS: LIST /////
interface UserListProps {
  users: User[];
}

function UsersList(props: UserListProps) {
  return <div>
    {
      props.users?.map((u: User, index: number) => {
        return <li key={index}>{u.name}</li>
      })
    }
  </div>
}

////// CHILD COMPONENTS: LIST COUNTER /////
function UsersListCounter() {
  const users = useRecoilValue(currentUsersState)

  return <div>
    Total users: {users.length}
  </div>
}
