import React  from 'react';
import { atom, atomFamily, selectorFamily, useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil';

/**
 * Description
 * Use selectorFamily to handle local states to a series of similar elements
 */
const elementPositionState = atom<{ [key:string]: number }>({
  key: 'ElementPosition2',
  default: {
    1: 100,
    2: 200,
    3: 300
  }
});

const elementPositionStateFamily = selectorFamily<number,  number>({
  key: 'ElementPosition',
  get: (id) => ({get}) => {
    return get(elementPositionState)[id] * 10;
  },

  set: (id) => ({set}, newValue) => {
    set(elementPositionState, prevState => ({...prevState, [id]: +newValue * 1000}) );
  },
});

function ElementListItem({elementID} : { elementID: number}) {
  const position = useRecoilValue(elementPositionStateFamily(elementID));
  const setPosition = useSetRecoilState(elementPositionStateFamily(elementID));

  console.log('render', elementID)
  return (
    <div>
      Element: {elementID}
      Position: {position}
      <button onClick={() => setPosition(Math.random())}> Random Value</button>
    </div>
  );
}

export function Demo12() {
  return <div>
    <ElementListItem elementID={1} />
    <ElementListItem elementID={2} />
  </div>
}

